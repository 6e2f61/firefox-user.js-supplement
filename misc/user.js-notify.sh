#!/usr/bin/env bash
set -h -u -o 'pipefail'

## SCRIPT INFORMATION
# ==============================================================================
# Name          : Firefox/Thunderbird Config Update Notifier
# Description   : This script performs the following operations...
#               : * compare versions of local and remote 'arkenfox' user.js for
#               :   Firefox and the 'HorlogeSkynet' user.js for Thunderbird
#               : * compare versions of local and remote user-overrides.js files
#               :   (by 12bytes.org) for both Firefox and Thunderbird
#               : * check the last modified date for The Firefox Privacy Guide
#               :   For Dummies!, the Firefox Configuration Guide for Privacy
#               :   Freaks and Performance Buffs and the Thunderbird Privacy
#               :   Guide for Dummies! from 12bytes.org
#               : * display a desktop notification containing a compilation of
#               :   the information collected
# License       : Non-Profit Open Software License ("Non-Profit OSL") 3.0
# Requirements  : GNU/Linux OS, Bash compatible shell
# Dependencies  : curl, libnotify, pcre2
# Author        : 12bytes.org
# Code          : 12bytes.org/Firefox-user.js-supplement - Codeberg
#               : https://codeberg.org/12bytes/firefox-user.js-supplement
# Web           : The Firefox Privacy Guide For Dummies! - 12bytes.org
#               : https://12bytes.org/articles/tech/firefox/the-firefox-privacy-guide-for-dummies/ - 12Bytes.org
# Web           : Firefox Configuration Guide for Privacy Freaks and Performance Buffs - 12Bytes.org
#               : https://12bytes.org/articles/tech/firefox/firefoxgecko-configuration-guide-for-privacy-and-performance-buffs/
# Web           : The Thunderbird Privacy Guide for Dummies!
#               : https://12bytes.org/articles/tech/the-thunderbird-privacy-guide-for-dummies/
#
## USAGE
# ==============================================================================
# Edit the variables below then place this script somewhere, perhaps in your
# Home or Firefox profile directory, and make it executable. To run this script
# manually in a terminal:
# $ ./user.js-notify.sh
# To run this script automatically you can add it to your startup programs, as a
# CRON job, a systemd timer, etc.
#
## EDIT THE FOLLOWING VARIABLES
# ==============================================================================

# GENERAL OPTIONS
# ---------------
# how many seconds to wait after log-on before performing update checks.
# default value: 0
iSec=0

# FIREFOX OPTIONS
# ---------------
# the path to your Firefox profiles.ini file. only the default profile will be
# checked therefore there must be a default entry in the file such as
# "Default=<random_chars>.default". set this to '' if you don't want to perform
# any checks for Firefox.
# default value: "${HOME}/.mozilla/firefox/profiles.ini"
sFFProfileIni="${HOME}/.mozilla/firefox/profiles.ini"

# in addition to checling for an updated 'arkenfox' user.js, shall we check for
# an updated user-overrides.js by 12bytes.org as well? ('yes' or 'no').
# default value: 'yes'
sFFChkOverridesJs='yes'

# path to your user-overrides.js file relative to your Firefox profile directory.
sFFOverridesJsPath="user-overrides.js"

# THUNDERBIRD OPTIONS
# -------------------
# the path to your Thunderbird profiles.ini file. only the default profile will
# be checked therefore there must be a default entry in the file such as
# "Default=<random_chars>.default". set this to '' if you don't want to perform
# any checks for Thunderbird.
# default value: "${HOME}/.thunderbird/profiles.ini"
sTbProfileIni=""

# in addition to checling for an updated 'HorlogeSkynet' user.js, shall we check
# for an updated user-overrides.js by 12bytes.org as well? ('yes' or 'no').
# default value: 'yes'
sTbChkOverridesJs='no'

# ==============================================================================
## STOP EDITING

# DEVELOPER NOTES
# search for BUG, TODO

sScriptVersion='20240413' # TODO update version
sScriptUrl='https://codeberg.org/12bytes/Firefox-user.js-supplement/raw/branch/master/misc/user.js-notify.sh'
sScriptRelNotesUrl='https://codeberg.org/12bytes/firefox-user.js-supplement/commits/branch/master'
iUpdate=0

[[ "${iSec}" -gt 0 ]] && sleep "${iSec}"

# check for updated version of this script
if sRemoteScript="$(curl --fail --silent --connect-timeout 30 -- "${sScriptUrl}")"; then
	if sRemoteScriptVer="$(pcre2grep --ignore-case --only-matching=1 --max-count=1 '^sScriptVersion='\''([0-9]+)'\''' <<<"${sRemoteScript}")"; then
		if [[ "${sRemoteScriptVer}" != "${sScriptVersion}" ]]; then
			sNotify+='* UPDATE: Please update this user.js-notify.sh script:\n'
			sNotify+="Download URL: ${sScriptUrl}\n"
			sNotify+="Release notes: ${sScriptRelNotesUrl}\n"
			notify-send --urgency='normal' --app-name="user.js-notify.sh v${sScriptVersion}" 'Firefox/Thunderbird Config Checker' "${sNotify}"
			exit
		fi
	else
		sNotify+='ERROR: Failed to get version of remote user.js-notify.sh\n'
	fi
else
	sNotify+='ERROR: Failed to read remote user.js-notify.sh\n'
fi

f_Checker() {

	sNotify+='⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\n'
	sNotify+="${sApp} config checks\n"

	# get profile name and directory
	sProfile="$(pcre2grep --ignore-case --only-matching=1 --max-count=1 '^Default=([a-z0-9]+\.default.*)' "${sProfileIni}")"
	sNotify+="Profile: ${sProfile}\n"
	if [[ "${sApp}" = 'Thunderbird' ]]; then
		sProfileDir="${HOME}/.thunderbird/${sProfile}"
	else
		sProfileDir="${HOME}/.mozilla/firefox/${sProfile}"
	fi

	# get local user.js version
	if sLocalJsVer="$(pcre2grep --ignore-case --only-matching=1 --max-count=1 '^[ *]+version[ v:]+([0-9]+)' "${sProfileDir}/user.js")"; then
		sNotify+="Local user.js v${sLocalJsVer}\n"

		# get local and remote user-overrides.js versions
		if [[ "${sChkOverridesJs}" = 'yes' ]]; then

			# get local user-overrides.js version string
			if sLocalOverridesJsVer="$(pcre2grep --ignore-case --only-matching=1 --max-count=1 '^[ *]+version[ v:]+([0-9]+)' "${sProfileDir}/${sOverridesJsPath}")"; then
				sNotify+="Local user-overrides.js v${sLocalOverridesJsVer}\n"

				# read remote user-overrides.js
				if sRemoteOverridesJs="$(curl --fail --silent --connect-timeout 30 -- "${sUserOverridesJsUrl}")"; then

					# get remote version
					if sRemoteOverridesJsVer="$(pcre2grep --ignore-case --only-matching=1 --max-count=1 '^[ *]+version[ v:]+([0-9]+)' <<<"${sRemoteOverridesJs}")"; then

						# get remote user.js version
						if sRemoteJs="$(curl --fail --silent --connect-timeout 30 -- "${sUserJsUrl}")"; then

							# get remote user.js version
							if sRemoteJsVer="$(pcre2grep --ignore-case --only-matching=1 --max-count=1 '^[ *]+version[ v:]+([0-9]+)' <<<"${sRemoteJs}")"; then

								# compare local and remote user.js versions
								if [[ "${sLocalJsVer}" = "${sRemoteJsVer}" ]]; then
									sNotify+="Local and remote user.js versions match\n"
								else
									iUpdate=1
									sNotify+="* UPDATE: Remote user.js v${sRemoteJsVer} is available\n"
								fi

								# compare local and remote user-overrides.js versions
								if [[ "${sLocalOverridesJsVer}" = "${sRemoteOverridesJsVer}" ]]; then
									sNotify+="Local and remote user-overrides.js versions match\n"
								else
									iUpdate=1
									sNotify+="* UPDATE: Remote user-overrides.js v${sRemoteOverridesJsVer} is available\n"
								fi
							else
								sNotify+='ERROR: Failed to get remote user.js version\n'
							fi

						else
							sNotify+='ERROR: Failed to read remote user.js\n'
						fi
					else
						sNotify+='ERROR: Failed to get remote user-overrides.js version\n'
					fi
				else
					sNotify+='ERROR: Failed to read remote user-overrides.js\n'
				fi
			else
				sNotify+='ERROR: Failed to get local user-overrides.js version\n'
			fi
		fi
	else
		sNotify+='ERROR: Failed to get local user.js version\n'
	fi
}

if [[ -n "${sFFProfileIni}" ]]; then
	sApp='Firefox'

	# user set variables
	sProfileIni="${sFFProfileIni}"
	sChkOverridesJs="${sFFChkOverridesJs}"
	sOverridesJsPath="${sFFOverridesJsPath}"

	# internal variables
	sUserJsUrl='https://raw.githubusercontent.com/arkenfox/user.js/master/user.js'
	sUserJsRepoUrl='https://github.com/arkenfox/user.js'
	sUserOverridesJsUrl='https://codeberg.org/12bytes/Firefox-user.js-supplement/raw/branch/master/user-overrides.js'
	sUserOverridesJsRepoUrl='https://codeberg.org/12bytes/Firefox-user.js-supplement'

	f_Checker
fi

if [[ -n "${sTBProfileIni}" ]]; then
	sApp='Thunderbird'

	# user set variables
	sProfileIni="${sTBProfileIni}"
	sChkOverridesJs="${sTBChkOverridesJs}"
	sOverridesJsPath="${sTBOverridesJsPath}"

	# internal variables
	sUserJsUrl='https://raw.githubusercontent.com/HorlogeSkynet/thunderbird-user.js/master/user.js'
	sUserJsRepoUrl='https://github.com/HorlogeSkynet/thunderbird-user.js'
	sUserOverridesJsUrl='https://codeberg.org/12bytes/thunderbird-user.js-supplement/raw/branch/master/user-overrides.js'
	sUserOverridesJsRepoUrl='https://codeberg.org/12bytes/thunderbird-user.js-supplement'

	f_Checker
fi

# send notification
[[ "${iUpdate}" -eq 1 ]] && sNotify="AN UPDATE IS AVAILABLE!\n${sNotify}"
notify-send --urgency='normal' --app-name="user.js-notify.sh v${sScriptVersion}" 'Firefox/Thunderbird Config Checker' "${sNotify}" &

exit
