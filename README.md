# 12bytes.org user-overrides.js supplement for the 'arkenfox' user.js for Firefox

## IMPORTANT

**this user-overrides.js is an optional supplement intended to be appended to the [arkenfox user.js file](https://github.com/arkenfox/user.js)** (a security and privacy centric user preferences file for Firefox and its derivatives) and used in conjunction with the ['Firefox Configuration Guide for Privacy Freaks and Performance Buffs'](https://12bytes.org/firefox-configuration-guide-for-privacy-freaks-and-performance-buffs/) or [The Firefox Privacy Guide For Dummies!](https://12bytes.org/the-firefox-privacy-guide-for-dummies/)

the preferences in the user-overrides.js are my personal preferences which are not edited for public consumption, therefore you will need to adjust as necessary

## IMPLEMENTATION

1. read the Firefox Configuration Guide for Privacy Freaks and Performance Buffs or The Firefox Privacy Guide For Dummies!
2. add the appropriate files to your Firefox profile from the [arkenfox repo](https://github.com/arkenfox/user.js), including the their user.js, updater.sh (Linux) or updater.bat (Windows), and prefsCleaner.sh (Linux) or prefsCleaner.bat (Windows) - don't forget to make the .sh scripts executable if you're running Linux
3. download the user-overrides.js file to your Firefox profile directory
4. edit the settings in user-overrides.js to suit your personal preferences - instructions are contained in the file
5. use the 'arkenfox' updater script to append the user-overrides.js file to the user.js file - for Linux, in a terminal run:
./updater.sh -c

## UPDATE NOTIFICATIONS

* to be notified of updates to the 'arkenfox' user.js, subscribe to the [commits feed](https://github.com/arkenfox/user.js/commits/master) and/or use the user.js-version-checker.sh script in the /misc directory (instructions below)
* to be notified of updates to the user-overrides.js in this repository, [subscribe](https://codeberg.org/12bytes/firefox-user.js-supplement.rss) to the news feed for this repository
* to see the changes between versions, click on the [commits](https://codeberg.org/12bytes/firefox-user.js-supplement/commits/branch/master) link, then click on the latest version to compare it with the previous version

### USING THE USER.JS-NOTIFY.SH SCRIPT

the purpose of the user.js-notify.sh script in the /misc directory is to automatically check whether a new version of the 'arkenfox' user.js and, optionally, the user-overrides.js in this repo are available and display a desktop notification. to use the script, drop it in the Firefox profile directory along side the 'arkenfox' user.js file, then open it and edit the path to your Firefox profile directory and, optionally, enable the update check for the user-overrides.js if you're using it. save the file and make it executable. to run the script automatically, you could add it to your list of startup programs, or as a CRON job, a systemd timer, etc.
